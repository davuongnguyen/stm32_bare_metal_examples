/*
 * d_macro.h
 *
 *  Created on: Dec 12, 2023
 *      Author: davuo
 */

#ifndef D_MACRO_H_
#define D_MACRO_H_





#define __O     volatile             /*!< Defines 'write only' permissions */
#define __IO    volatile             /*!< Defines 'read / write' permissions */





#endif /* D_MACRO_H_ */
