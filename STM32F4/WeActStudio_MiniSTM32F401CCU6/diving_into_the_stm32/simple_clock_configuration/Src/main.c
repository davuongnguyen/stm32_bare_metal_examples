/**
 ******************************************************************************
 * @file           : main.c
 * @author         : Auto-generated by STM32CubeIDE
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */

#include <stdint.h>
#include "stm32f401xc.h"
#include "gpio.h"
#include "mco.h"

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void);

int main(void)
{
    /* Configure the system clock */
    SystemClock_Config();
    
    /* Configure MCO */
    MCO_Config();

    /* Configure GPIOs */
    GPIO_Config();
    
    /* Loop forever */
	for(;;);
}

void SystemClock_Config(void)
{
    // Configure PLL clock source and multiplication factor
    RCC->CR |= (0b1 << 0);                          // HSION = 1, turn on HSI oscillator
    while((RCC->CR & (0b1 << 1)) != (0b1 << 1));    // Wait until HSI is ready

    RCC->CFGR |= (0b00 << 0);                       // SW = 0b00, select HSI as system clock                
}
