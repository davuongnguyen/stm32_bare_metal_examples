# STM32 Bare Metal Examples

Đây là kho lưu trữ tổng hợp mã nguồn ví dụ mô tả trong cuốn sách [Learn STM32 Bare Metal]().

|       Nội dung        |                            | STM32F103 |                          STM32F401                           | STM32H750 | STM32H723 |
| :-------------------: | :------------------------- | :-------: | :----------------------------------------------------------: | :-------: | :-------: |
|                       | Simple Memory Map          |     ❌     | [✅](./STM32F4/WeActStudio_MiniSTM32F401CCU6/diving_into_the_stm32/simple_memory_map/) |     ❌     |     ❌     |
| Diving into the STM32 | Simple Clock Configuration |     ❌     | [✅](./STM32F4/WeActStudio_MiniSTM32F401CCU6/diving_into_the_stm32/simple_clock_configuration/) |     ❌     |     ❌     |
|                       | Control LED                |     ❌     | [✅](./STM32F4/WeActStudio_MiniSTM32F401CCU6/diving_into_the_stm32/control_led/) |     ❌     |     ❌     |

